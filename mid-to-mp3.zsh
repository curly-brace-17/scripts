# requires: timidity, ffmpeg
# source: https://wiki.archlinux.org/title/Timidity%2B%2B#Convert_files

timidity $1 -Ow -o - | ffmpeg -i - -acodec libmp3lame -ab 256k $2
